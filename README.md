# README #

### What is this repository for? ###

* This is a telegram bot helper template
* 1.0.0
* Refer to telegram_bot_guide.docx for more info

### How do I get set up? ###

* Go through the template, it is very simple to understand and will get you started with building a useful telegram bot in no time

### What is heroku? ###
* http://heroku.com/ is a site which provides PaaS for users for free.
* In Free mode: It creates servers which can be executed continuously for a while before the server is put to sleep.
* The setup is simple
* Signup here: https://signup.heroku.com/
* Download https://devcenter.heroku.com/articles/heroku-cli

### Deploying to heroku? ###
* A brief setup description https://devcenter.heroku.com/articles/deploying-nodejs
* Since this application needs environment variables, post heroku login we would need to set these. Reference link https://devcenter.heroku.com/articles/config-vars
* heroku config:set TELEGRAM_TOKEN=******
* heroku config:set MONGODB_URI=******
* To get a free mongo DB uri for above go https://elements.heroku.com/addons/mongolab

### Who do I talk to? ###

* Prabjeet Singh Pahwa (ppahwa@deloitte.com)
* Saurabh S Shah (saushah@deloitte.com)
